let posiciones = [1, 5, 9, 8, 7, 3, 2];
let total = 0;

total = productoPares(posiciones);
console.log(total);

total = sumaImpares(posiciones);
console.log(total);

function productoPares(numeros) {
    let resultado = 1;
    for (let cont = 0; cont < numeros.length; cont++) {
        if (numeros[cont] % 2 == 0) {
            resultado *= numeros[cont];
        }
    }
    return resultado;
}

function sumaImpares(numeros) {
    let resultado = 0;
    for (let cont = 0; cont < numeros.length; cont++) {
        if (numeros[cont] % 2 != 0) {
            resultado += numeros[cont];
        }
    }
    return resultado;
}

