let valores = [];
let r = 0;

// Asigno los valores del array con un for para no escribir 25 numeros a mano.
for (let cont = 0; cont <= 25; cont++) {
    valores[cont] = cont;
}

// Creamos la funcion que use el valor de 'numeros' para que retorne el resultado del for, en este caso retorna la suma de todos los numeros apres contenidos en 'numeros'.
function suma(numeros) {
    for (let cont = 0; cont < numeros.length; cont++) {
        if (numeros[cont] % 2 == 0) {
            r += numeros[cont];
        }
    }
    return r;
}

// Se le asignan los valores de la variable 'valores' para que la funcion la reemplace por su valor de 'numeros' y se iguala a r.
r = suma(valores);

// Ahora r es igual al resultado de la funcion asi que la imprimimos.
document.write(`El resultado de la suma de numeros pares del ${valores[0]} al ${valores[valores.length - 1]} es: ${r}`);