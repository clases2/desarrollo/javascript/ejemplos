function escribir(valor) {
    let display = document.querySelector('.display');
    let derecha = window.getComputedStyle(display).getPropertyValue('left'); // Leemos la propiedad left que es la posicion X del elemento para mvoerse horizontalmente.
    let abajo = window.getComputedStyle(display).getPropertyValue('top'); // Leemos la propiedad top que es la posicion Y del elemento para mvoerse verticalmente.

    if (valor == '1') {
        display.innerHTML = display.innerHTML + 1;
    } else if (valor == '2') {
        display.innerHTML = display.innerHTML + 2;
    } else if (valor == '3') {
        display.innerHTML = display.innerHTML + 3;
    } else if (valor == 'c') {
        display.innerHTML = ``;
    } else if (valor == 'bajar') {
        display.style.top = parseInt(abajo) + 10 + "px";
    } else if (valor == 'mover') {
        display.style.left = parseInt(derecha) + 10 + "px";
    }
}