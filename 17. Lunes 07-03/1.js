window.addEventListener("load", function () {
    // Este codigo se ejecuta cuando termine de cargar la pagina.


    document.querySelector('button').addEventListener("click", function () {
        //Este codigo se ejecuta cuando pulso el boton.
        //document.querySelector('#salida').innerHTML+=`<div>Hola mundo</div>`; Esto seria hasta ahora.

        //creo un div.
        let div = document.createElement("div");
        //creo un nodo de texto
        let texto = document.createTextNode("Hola mundo");

        // Formas de añadir texto al div.
        //div.innerHTML = `Hola mundo.`;
        //div.textContent=`Hola mundo`;
        div.appendChild(texto);

        document.querySelector('#salida').appendChild(div);

    });
});