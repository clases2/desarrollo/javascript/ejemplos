let texto = [
    "uno",
    "dos",
    [
        "CINCO",
        "sub1",
        "sub2"
    ],
    "tres",
    [
        "cuatro",
        "sub1",
        "sub2"
    ]
];

const UL = document.createElement("ul");

for (let c = 0; c < texto.length; c++) {
    const LI = document.createElement("li");
    const A = document.createElement("a");
    A.href = "#";
    LI.appendChild(A)

    if (texto[c] instanceof Array) {
        const SUL = document.createElement("ul");
        A.textContent = `${texto[c][0]}`

        for (let c1 = 1; c1 < texto[c].length; c1++) {
            const SLI = document.createElement("li");
            const SA = document.createElement("a");

            SA.href = "#";
            SLI.appendChild(SA)
            SA.textContent = `${texto[c][c1]}`

            SUL.appendChild(SLI);
        }
        LI.appendChild(SUL);

    } else {

        A.textContent = `${texto[c]}`
    }

    UL.appendChild(LI);
}





document.body.appendChild(UL);