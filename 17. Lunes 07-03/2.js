window.addEventListener("load", function () {
    // Este codigo se ejecuta cuando termine de cargar la pagina.
    let div = document.createElement("div");
    div.innerHTML = `Hola mundo.`;

    document.querySelector('button').addEventListener("click", function () {
        //cada vez que se presiona el boton se coloca en salida un clon del div original.
        document.querySelector('#salida').appendChild(div.cloneNode(true));
    });
});