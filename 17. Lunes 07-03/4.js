document.querySelector('button').addEventListener("click", function () {
    //Creo un fragmento de codigo.
    const FRAGMENTO = document.createDocumentFragment();
    //Creo una etiqueta div.
    const ETIQUETA = document.createElement("div");

    for (let c = 1; c <= 3; c++) {
        //Creo una etiqueta img.
        const FOTO = document.createElement("img");
        //Coloco el src de la imagen.
        FOTO.src = `../16. Jueves 03-03/img/${c}.jpg`
        //Añado la foto al fragmento.
        FRAGMENTO.appendChild(FOTO);
    }
    //Añado al div el fragmento.
    document.querySelector('#dfoto').appendChild(FRAGMENTO);
});
