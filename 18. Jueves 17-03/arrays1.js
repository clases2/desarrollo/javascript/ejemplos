let divs = document.querySelectorAll('div');

// Utilizamos un for
for (let c = 0; c < divs.length; c++) {
    console.log(divs[c].innerHTML);
}

// Utilizamos el for of
for (let valor of divs) {
    console.log(valor.innerHTML);
}

// Utilizamos el for in
for (const indice in divs) {
    console.log(divs[indice].innerHTML);
}

// Utilizamos el foreach
divs.forEach(function (valor, indice) {
    console.log(valor.innerHTML);
});