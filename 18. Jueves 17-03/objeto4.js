/* La funcion rebibe una serie de numeros
{
    vector1:[1, 2, 5, 2, 12, 3, 5];
    vector2:[2, 4, 1, 9, 2, 74, 6];
    salida:"texto";
}
La funcion me tiene que devolver u objeto de tipo
{
    suma:[vector1+vector2];
    producto:[vector1*vector2];
    max:numero mas grande de todos los vectores;
}
me debe mostra el valor mas grande en el elemento con id 'salida'
*/

let origen = {
    vector1: [1, 2, 5, 2, 12, 3, 5],
    vector2: [2, 4, 1, 9, 2, 74, 6],
    salida: "salida"
}

function operaciones(datos) {
    let resultado = {
        suma: [],
        producto: [],
        max: 0
    }

    for (let i in datos.vector1) {
        resultado.suma[i] = +datos.vector1[i] + datos.vector2[i];
        resultado.producto[i] = datos.vector1[i] * datos.vector2[i];
    }

    resultado.max = Math.max(...datos.vector1, ...datos.vector2); // La expresion '...' toma todos los valores de un array.

    document.querySelector(`#${datos.salida}`).innerHTML = `${resultado.max}`;
    return resultado
}
console.log(origen);
console.log(operaciones(origen));