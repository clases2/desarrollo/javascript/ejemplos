/* recibe un objeto de tipo
{
    color:"red",
    radio: 40,
    borde: 2,
    colorborde:"blue",
    x: 15,
    y: 20
}
*/

let datoscirculo = {
    color: "red",
    x: 15,
    y: 20,
    radio: 40,
    borde: 2,
    colorborde: "black"
}

function circulo(datos) {
    let div = document.createElement("div");

    div.style.left = `${datos.x}px`;
    div.style.top = `${datos.y}px`;
    div.style.backgroundColor = `${datos.color}`;
    div.style.width = `${datos.radio * 2}px`;
    div.style.height = `${datos.radio * 2}px`;
    div.style.border = `${datos.borde}px ${datos.colorborde} solid`;
    div.style.borderRadius = `${datos.radio}px`;
    div.style.position = `absolute`;

    document.body.appendChild(div);
}

circulo(datoscirculo);