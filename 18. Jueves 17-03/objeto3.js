/* La funcion recibe un string, la funcion me cuenta cuantas veces se repite cada vocal, me retorna un objeto e este tipo:
{
a:0,
e:0,
i:0,
o:0,
u:0
}
*/

function contar(texto) {
    let vocales = {
        a: 0,
        e: 0,
        i: 0,
        o: 0,
        u: 0
    }

    texto = texto.toLowerCase();

    for (let letra of texto) {
        if (letra == 'a') {
            vocales.a += 1;
        } else if (letra == 'e') {
            vocales.e += 1;
        } else if (letra == 'i') {
            vocales.i += 1;
        } else if (letra == 'o') {
            vocales.o += 1;
        } else if (letra == 'u') {
            vocales.u += 1;
        }
    }

    return vocales;
}

console.log("Ejemplo de clase");
console.log(contar("Ejemplo de clase"));