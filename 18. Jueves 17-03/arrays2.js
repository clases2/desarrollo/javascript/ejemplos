let vector = [23, 'ejemplo',];

// Añadir una propiedad al array
vector.color = 'rojo';

// Utilizando for
for (let c = 0; c < vector.length; c++) {
    console.log(vector[c]);
}

// Utilizando foreach
vector.forEach(function (valor, indice) {
    console.log(indice, valor);
});

// Utilizando for of
for (let [indice, valor] of vector.entries()) {
    console.log(indice, valor);
}

// Utilizando for in
for (let indice in vector) {
    console.log(indice, vector[indice]);
}