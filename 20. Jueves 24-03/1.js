let Numeros = function (datos) {
    this.valores = datos;
    this.maximo = 0;
    this.minimo = 0;
    this.mostrar = function () {
        // miembro privado
        let texto = "";
        this.valores.forEach((elemento) => {
            texto += "<li>" + elemento + "</li>";
        });
        return texto;
    };

    this.calcular = function () {
        this.maximo = Math.max(...this.valores);
        this.minimo = Math.min(...this.valores);
    };
}

let objeto = new Numeros([1, 34, 56, 78]);
document.write(objeto.mostrar());
objeto.calcular();
document.write("El valor mayor es:" + objeto.maximo);
document.write("El valor menor es:" + objeto.minimo);