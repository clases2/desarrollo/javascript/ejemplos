// clase
// funcion constructora
let Caja=function(an=0,al=0,c="#CCC"){
	this.ancho=an;
	this.alto=al;
	this.color=c;

	this.dibujar=function(){
		return `<div style="width:${this.ancho}px;height:${this.alto}px;background-color:${this.color}"> </div>`
	}

}

// objeto
// es de tipo Caja
let caja1=new Caja(23,45,"RED");

document.write(caja1.dibujar());

document.write(caja1.dibujar());

let caja2=new Caja(10,10,"blue");
document.write(caja2.dibujar());


