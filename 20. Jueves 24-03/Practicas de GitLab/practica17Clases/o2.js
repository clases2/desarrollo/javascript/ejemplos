class Animal {
    //Propiedad estatica
    static pelo = "si";

    //Metodo estatico
    static estatico = function () {
        console.log("esto es un elemento de la clase");
        /* dentro de un elemento estatico no puedo llamar a elementos no estaticos 
        this.comer();
        */
        console.log(Animal.pelo);
    };

    constructor() {
        //Propiedad publica.
        this.peso = 0;
    }

    //Metodo publico
    comer() {
        console.log("ÑAN");
    };
    tienePelo() {
        /* dentro de un metodo no estatico si podemos acceder a miembros estaticos */
        console.log("tiene pelo??" + Animal.pelo);
    };
};

const leon = new Animal();
/* Produce error ya que es un metodo de la clase y no del objeto 
leon.estatico();
*/
Animal.estatico();
console.log(Animal.pelo);
leon.tienePelo();
/* Esto esta vacio porque es un elemento de la clase y no del objeto
console.log(leon.pelo);
*/