window.addEventListener("load",function(){
    let unPerro=new Perro();
    document.querySelector('button').addEventListener("click",function(){
    
        unPerro.ladrar();
    });

});


class Perro{
    #ladrido=null;
    constructor(){
        this.sonido="perro.mp3";
        this.img=document.createElement("img");
        document.body.appendChild(this.img);
        this.img.style.width="50px";
        this.img.src="perro.png";
        this.#ladrido=new Audio(this.sonido);
    }

    ladrar(){
        this.#ladrido.play();
    }
   
}


