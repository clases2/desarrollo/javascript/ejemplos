class Animal {
    constructor() {
        this.nombrePadre = "soy el padre";
    }
};

// Una forma de añadir los miembros de Animal a Leon.
class Leon extends Animal {
    constructor() {
        super();
        this.nombreHijo = "soy el hijo";
    }
};

const objetoPadre = new Animal();
const objetoHijo = new Leon();

console.log(objetoPadre.nombrePadre);
console.log(objetoHijo.nombrePadre);
console.log(objetoHijo.nombreHijo);