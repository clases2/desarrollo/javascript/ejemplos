/* Vamos a crear una clase */

class Persona {
    // Propiedad privada
    #nombre = "";
    // Metodos publicos
    dormir = function () {
        console.log("ZZZZZZZZZZZZ");
    };

    hablar = function () {
        console.log("BLA BLA BLA");
    };

    contar = function () {
        console.log("1 2 3 4 5 6");
    };

    get nombre() {
        return this.#nombre;
    }

    set nombre(valor) {
        this.#nombre = valor;
    }
};

/* Vamos a crear un objeto con una instancia de la clase */
const alumno = new Persona();

alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.nombre = "BurguerMcBurguer";
console.log(alumno.nombre);