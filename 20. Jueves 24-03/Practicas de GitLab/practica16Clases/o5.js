/* 
    Crear un objeto con la clase Object
*/

const persona = new Object();
persona.edad = 10;
persona.nombre = "ramon";
persona.decirNombre = function () {
    console.log(this.nombre);
};

/*
    accedemos a los elementos del objeto
*/

console.log(persona.nombre);
persona.decirNombre();

/* Esto no se puede 
 
hijo=new persona();
hijo.decirNombre();
*/