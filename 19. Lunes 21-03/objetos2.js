let loro = {
    color: "azul",
    peso: 10,
    habar: function () {
        return "piopio";
    },
    volar: function () {
        return "volando voy";
    }
};

// Cuidado, estamos pasando el loro por referencia. Las dos variables usan el mismo objeto, no se clona.
let loro1 = loro;

loro.plumas = true;
console.log(loro1.plumas);