/* Objeto con los siguientes miembros.
objeto loro
    MIEMBROS   
        propiedades
            color
            peso
        metodos
            hablar
            volar
*/

// Notacion json
let loro = {
    color: "azul",
    peso: 10,
    habar: function () {
        return "piopio";
    },
    volar: function () {
        return "volando voy";
    }
};

let loroArray = [
    "azul",
    10,
    function () { return "piopio" },
    function () { return "volando voy" }
];

// Propiedad
console.log(`El color de mi loro es ${loro.color}`);
// Metodo
console.log(loro.volar());

// Acceder a los elementos del array
console.log(`El color de mi loro es ${loroArray[0]}`);
console.log(loroArray[3]());

// Modificar el color de mi loro
loro.color = "rojo";