/* Creando clase denominada Perro con metodos antigulo y nuevo.
propiedades
    color
    pelo
    peso
    fechaNacimiento
metodos
    ladrar ("gua gua")
    dormir ("durmiendo")
*/

// Metodo antiguo.
let Perro1 = function () {
    this.constructor = function () {
        this.color = "blanco";
        this.pelo = "corto";
        this.peso = 30;
        this.fechaNacimiento = "18/06";
    }

    this.ladrar = function () {
        return "gua gua";
    }
    this.dormir = function () {
        return "durmiendo";
    }

    this.constructor();
}

// Metodo nuevo.
class Perro2 {
    constructor() {
        this.color = "negro";
        this.pelo = "largo";
        this.peso = "40";
        this.fechaNacimiento = "04/11";
    }

    ladrar() {
        return "gua gua";
    }
    dormir() {
        return "durmiendo";
    }
}

let perroViejo = new Perro1;
let perroNuevo = new Perro2;