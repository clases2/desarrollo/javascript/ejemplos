// Creando una clase de tipo Loro.
let Loro = function () {
    // Creando propiedades publicas.
    // Metodo constructor.
    this.constructor = function () {
        console.log(`Creando loro`);
        this.color = "rojo";
        this.peso = 10;
    }
    // Creando metodos publicos.
    this.hablar = function () {
        return "piopio";
    }
    this.volar = function () {
        return "volando voy";
    }

    // Llamano al constructor.
    this.constructor();
};

//Creo dos objetos de tipo Loro.
let loro1 = new Loro;
let loro2 = new Loro;

loro1.color = "azul";
