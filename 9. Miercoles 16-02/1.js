// Creando un array
let vector = [];

// Introducor con un bucle datos al vector
for (let contador = 0; contador < 10; contador++) {
    vector[contador] = contador;
}

document.write(vector);

// Recorriendo el array para mostrarlo
for (let contador = 0; contador < vector.length; contador++) {
    document.write(`<div>${vector[contador]}</div>`);
}

// Vamos a crear un array con los li
let elementos = document.querySelectorAll('li');
for (let contador = 0; contador < 5; contador++) {
    elementos[contador].innerHTML += contador;
    elementos[contador].style.backgroundColor = "GRAY";
    elementos[contador].style.border = "2px solid red";
}