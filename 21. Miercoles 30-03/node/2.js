const express = require('express');
const app = express();
const router = express.Router();
const fs = require('fs');

router.get('/home', (req, res) => {
  fs.readFile('./a.html', (err, data) => {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.write(data.toString())
    res.end();
  });
});

router.get('/profile', (req, res) => {
  res.send('Hello World, This is profile router');
});

router.get('/login', (req, res) => {
  res.send('Hello World, This is login router');
});

router.get('/logout', (req, res) => {
  res.send('Hello World, This is logout router');
});

app.use('/', router);

app.listen(process.env.port || 4000);

console.log('Web Server is listening at port ' + (process.env.port || 4000));