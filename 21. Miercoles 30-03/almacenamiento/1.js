let divs = document.querySelectorAll('div');
let agregar = document.querySelector('#agregar');
let borrar = document.querySelector('#borrar');
let media = [];

divs[0].innerHTML = localStorage.getItem('dato');

agregar.addEventListener('click', function () {
    let numero = +document.querySelector('#numero').value;
    let numeros = "";

    //Guardamos los datos para que no se pierdan al refrescar la pagina.
    if (localStorage.getItem('dato') != null) {
        numeros = `${localStorage.getItem('dato')}, ${numero}`;
    } else {
        numeros = numero;
    }

    localStorage.setItem('dato', numeros);
    divs[0].innerHTML = `${numeros}, `;

    //Calculamos la media.
    let suma = 0;
    let resultado = 0;
    media.push(numero);
    media.forEach(function (v) {
        suma += v;
    });

    resultado = suma / media.length;

    divs[1].innerHTML = `La media de los numeros introducidos es de: ${resultado}.`;
})

borrar.addEventListener('click', function () {
    localStorage.removeItem('dato');
    divs[0].innerHTML = localStorage.getItem('dato');
})