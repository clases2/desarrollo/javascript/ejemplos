let divs = document.querySelectorAll('div');
let agregar = document.querySelector('#agregar');
let borrar = document.querySelector('#borrar');

divs[0].innerHTML = localStorage.getItem('dato');

agregar.addEventListener('click', function () {
    let numero = +document.querySelector('#numero').value;
    let numeros = [];

    //Guardamos los datos para que no se pierdan al refrescar la pagina.
    if (localStorage.getItem('dato') != null) {
        numeros = JSON.parse(localStorage.getItem('dato'));
    }

    numeros.push(numero);

    localStorage.setItem('dato', JSON.stringify(numeros));
    divs[0].innerHTML = `${numeros}`;

    //Calculamos la numeros.
    let suma = 0;
    let resultado = 0;
    numeros.forEach(function (v) {
        suma += v;
    });

    resultado = suma / numeros.length;

    divs[1].innerHTML = `La media de los numeros introducidos es de: ${resultado}.`;
})

borrar.addEventListener('click', function () {
    localStorage.removeItem('dato');
    divs[0].innerHTML = localStorage.getItem('dato');
    divs[1].innerHTML = localStorage.getItem('dato');
})