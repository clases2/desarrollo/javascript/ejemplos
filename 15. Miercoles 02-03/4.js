let tds = document.querySelectorAll('td');

/*for (let c = 0; c < tds.length; c++) {
    tds[c].addEventListener("click", function (e) {
        e.target.style.backgroundColor = 'red';
    })
}*/

for (let td of tds) { // Otro metodo de for para recorrer arrays.
    td.addEventListener("click", function (e) {
        e.target.style.backgroundColor = 'red';
    })
}

/* for (let c = 0; c < tds.length; c++) { // Cambiar el color a rojo o si ya esta rojo cambiarlo al color de antes.
    tds[c].addEventListener("click", function (e) {
        if (e.target.style.backgroundColor != 'red') {
            e.target.style.backgroundColor = 'red';
        } else {
            e.target.style.backgroundColor = 'cornflowerblue';
        }
    })
}
*/