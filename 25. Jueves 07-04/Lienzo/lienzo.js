let pacman = document.querySelector('#pacman');
let estilos = window.getComputedStyle(pacman);
let lienzo = document.querySelector('#lienzo');
let estilienzo = window.getComputedStyle(lienzo);
let direccion = "derecha";
const PASO = 10;

pacman.style.left = estilos.left;
pacman.style.top = estilos.top;
pacman.style.transform = estilos.transform;

// Movimiento del pacman
let intervalo = setInterval(function () {
    switch (direccion) {
        case 'derecha':
            pacman.style.left = `${parseInt(pacman.style.left) + PASO}px`;
            pacman.style.transform=`rotate(0deg)`;
            if (parseInt(pacman.style.left) >= 500 - 50) {
                direccion = "izquierda";
                /*alert("perdiste");
                clearInterval(intervalo) */
            };
            break;

        case 'izquierda':
            pacman.style.left = `${parseInt(pacman.style.left) - PASO}px`;
            pacman.style.transform=`rotate(180deg)`;
            if (parseInt(pacman.style.left) < 0+10) {
                direccion = "derecha";
                /*alert("perdiste");
                clearInterval(intervalo) */
            };
            break;
        case 'arriba':
            pacman.style.top = `${parseInt(pacman.style.top) - PASO}px`;
            pacman.style.transform=`rotate(270deg)`;
            if (parseInt(pacman.style.top) < 0+10) {
                direccion = "abajo";
                /*alert("perdiste");
                clearInterval(intervalo) */
            };
            break;

        case 'abajo':
            pacman.style.top = `${parseInt(pacman.style.top) + PASO}px`;
            pacman.style.transform=`rotate(90deg)`;
            if (parseInt(pacman.style.top) >= 500 - 50) {
                direccion = "arriba";
                /*alert("perdiste");
                clearInterval(intervalo) */
            };
            break;
    };
}, 50);

window.addEventListener("keydown", function (e) {
    switch (e.key) {
        case "ArrowDown":
            direccion = "abajo";
            break;
        case "ArrowUp":
            direccion = "arriba";
            break;
        case "ArrowLeft":
            direccion = "izquierda";
            break;
        case "ArrowRight":
            direccion = "derecha";
            break;
    }
});