let personas = [];
class Persona {
    constructor(id = 0, edad = 0, nombre = "ninguno", pob = "ninguna", mail = "ninguno") {
        this.id = id;
        this.nombre = nombre;
        this.mail = mail;
        this.edad = edad;
        this.pob = pob;
    };
};
/* let persona = {
    id: "",
    nombre: "",
    mail: "",
    edad: 0,
    pob: ""
}; */

window.addEventListener("load", function () {
    let fid = document.querySelector('#id');
    let fnombre = document.querySelector('#nombre');
    let fmail = document.querySelector('#mail');
    let fedad = document.querySelector('#edad');
    let fpob = document.querySelector('#poblacion');
    let tabla = document.querySelector('tbody');
    let c = 1;


    document.querySelector('#enviar').addEventListener("click", function () {
        let persona = new Persona();

        tabla.innerHTML += `
        <tr>
            <td>${c}</td>
            <td>${fid.value}</td>
            <td>${fnombre.value}</td>
            <td>${fmail.value}</td>
            <td>${fedad.value}</td>
            <td>${fpob.value}</td>
        </tr>`;
        c++

        persona.id = fid.value;
        persona.nombre = fnombre.value;
        persona.mail = fmail.value;
        persona.edad = fedad.value;
        persona.pob = fpob.value;

        personas.push(persona);
    });

    document.querySelector('#limpiar').addEventListener("click", function () {
        c = 1
        fid.value = "";
        fnombre.value = "";
        fmail.value = "";
        fedad.value = "";
        fpob.value = "";
        tabla.innerHTML = "";
    });
});