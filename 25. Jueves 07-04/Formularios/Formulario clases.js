class Registro {
    constructor() {
        this.personas = [];
    }
    mostrar(etiquetap = "tr", etiquetah = "td", inicio = 0, fin = this.personas.length) {
        let resultado = "";
        for (let i = inicio; i < fin; i++) {
            let reg = this.personas[i].imprimir(etiquetah);
            resultado += `<${etiquetap}>${reg}</${etiquetap}>`;
        };
        return resultado;
    };
};
const registro = new Registro;
class Persona {
    constructor(direccion = "ninguna", numero = "0", pob = "ninguna", puerta = "ninguna") {
        this.numero = numero;
        this.puerta = puerta;
        this.direccion = direccion;
        this.pob = pob;
    };

    imprimir(etiqueta) {
        let resultado = ``;
        resultado += `
        <${etiqueta}>${this.numero}</${etiqueta}>
        <${etiqueta}>${this.puerta}</${etiqueta}>
        <${etiqueta}>${this.direccion}</${etiqueta}>
        <${etiqueta}>${this.pob}</${etiqueta}>`;
        return resultado;
    }

    info(etiqueta) {
        let resultado = ``;
        resultado += `
        <${etiqueta}>
            Numero: ${this.numero}<br>
            Puerta: ${this.puerta}<br>
            Direccion: ${this.direccion}<br>
            Poblacion: ${this.pob}<br>
        </${etiqueta}>`;

        return resultado;
    }

    static tabla() {
        let th = "";
        let tr = ``;

        for (let titulo in new this()) {
            th += `<th>${titulo}</th>`;
        };

        tr = `<tr>${th}</tr>`;

        console.log(tr);

        return tr;
    };
};

/* Cambiamos el objeto por una clase.
    let persona = {
    numero: 0,
    puerta: "",
    direccion: "",
    pob: ""
}; */

window.addEventListener("load", function () {
    document.querySelector('thead').innerHTML = Persona.tabla();

    let fnumero = document.querySelector('#numero');
    let fpuerta = document.querySelector('#puerta');
    let fdireccion = document.querySelector('#direccion');
    let fpob = document.querySelector('#poblacion');
    let tbody = document.querySelector('tbody');
    let contenido = document.querySelector('#contenido');

    document.querySelector('#enviar').addEventListener("click", function () {
        const persona = new Persona();

        persona.numero = fnumero.value;
        persona.puerta = fpuerta.value;
        persona.direccion = fdireccion.value;
        persona.pob = fpob.value;

        /* Creamos un metodo estatico en la clase para imprimir una tabla con los resultados.
        tbody.innerHTML += Persona.tabla();

        tbody.innerHTML += `
        <tr>
            <td>${fnumero.value}</td>
            <td>${fpuerta.value}</td>
            <td>${fdireccion.value}</td>
            <td>${fpob.value}</td>
        </tr>`;
        
        contenido.innerHTML += `
          <p class="datos">
            numero: ${persona.numero}<br>
            puerta: ${persona.puerta}<br>
            direccion: ${persona.direccion}<br>
            poblacion: ${persona.pob}<br>
        </p>`;*/

        tbody.innerHTML = "";
        tbody.innerHTML += registro.mostrar();
        contenido.innerHTML += `${persona.info('nav')}`;
        registro.personas.push(persona);
    });

    document.querySelector('#limpiar').addEventListener("click", function () {
        tbody.innerHTML = "";
        contenido.innerHTML = "";
    });

    document.querySelector('#todos').addEventListener("click", function () {
        tbody.innerHTML = "";
        tbody.innerHTML += registro.mostrar();
    });

    document.querySelector('#algunos1').addEventListener("click", function () {
        tbody.innerHTML = "";
        tbody.innerHTML += registro.mostrar("tr", "td", 0, 4);
    });

    document.querySelector('#algunos2').addEventListener("click", function () {
        tbody.innerHTML = "";
        tbody.innerHTML += registro.mostrar("tr", "td", 4, 8);
    });

    document.querySelector('#resto').addEventListener("click", function () {
        tbody.innerHTML = "";
        tbody.innerHTML += registro.mostrar("tr", "td", 8);
    });

});