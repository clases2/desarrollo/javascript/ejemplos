let cajas = document.querySelectorAll("div");

for (let [indice, caja] of cajas.entries()) {
    caja.addEventListener("click", function (e) {
        e.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
        e.target.innerHTML = "";
    });
}

/*let divs = document.querySelectorAll('div');

for (const div of divs) {
    div.addEventListener("click", function (evento) {
        evento.target.style.backgroundImage = `url(img/${evento.target.innerHTML}.jpg)`;
        evento.target.innerHTML = ``;
    });
}*/