let cajas = document.querySelectorAll("div");

cajas.forEach(function (valor, indice) {
    valor.addEventListener("click", function (evento) {
        if (evento.target.style.backgroundImage == "") {
            evento.target.style.backgroundImage = "url(img/" + (indice + 1) + ".jpg)";
            evento.target.innerHTML = "";
        } else {
            evento.target.style.backgroundImage = "";
            evento.target.style.backgroundColor = "red";
        }
    });
});