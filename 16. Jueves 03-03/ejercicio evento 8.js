let boton = document.querySelectorAll('button');

for (let c = 0; c < boton.length; c++) {
    boton[c].addEventListener("click", (e)=>{
        luz(c);
     });
};

function luz(valor) {
    let imagen = document.querySelector("img");
    if (valor == '0') {
        imagen.src = "img/on.gif";
        imagen.style.visibility = "visible";
    } else if (valor == '1') {
        imagen.src = "img/off.gif";
        imagen.style.visibility = "visible";
    } else if (valor == '2') {
        imagen.style.visibility = "hidden";
    }
}