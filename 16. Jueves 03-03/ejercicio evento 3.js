let cajas = document.querySelectorAll("div");
let imagenes = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg'];

for (let c = 0; c < cajas.length; c++) {
    cajas[c].addEventListener("click", function (e) {
        e.target.style.backgroundImage = `url(img/${imagenes[c]})`;
        e.target.innerHTML = "";
    });
}

/*let divs = document.querySelectorAll('div');

for (const div of divs) {
    div.addEventListener("click", function (evento) {
        evento.target.style.backgroundImage = `url(img/${evento.target.innerHTML}.jpg)`;
        evento.target.innerHTML = ``;
    });
}*/