function cambiar(operacion) {
    let salida = document.querySelector('#salida');
    let ancho = window.getComputedStyle(salida).getPropertyValue("width"); // Con esta propiedad se lee el ancho del div salida.

    if (operacion == 'rojo') {
        salida.style.backgroundColor = 'red';
    } else if (operacion == 'azul') {
        salida.style.backgroundColor = 'blue';
    } else if (operacion == 'verde') {
        salida.style.backgroundColor = 'green';
    } else if (operacion == 'aumentar') {
        salida.style.width = parseInt(ancho) + 10 + "px"; // Hay que agregar la medida px al final.
    } else if (operacion == 'reducir') {
        salida.style.width = parseInt(ancho) - 10 + "px";
    }
}