let n1 = document.querySelector('#numero1');
let n2 = document.querySelector('#numero2');
let sal = document.querySelector('#salida');
let res = document.querySelector('#resultado');

document.querySelector('#boton').addEventListener("click", calcular);

function calcular() {
    let resultado = (+n1.value) + (+n2.value);
    sal.innerHTML = `${resultado}`;
    res.value = `${resultado}`;
}