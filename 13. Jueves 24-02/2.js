let n1 = document.querySelector('#numero1');
let n2 = document.querySelector('#numero2');
let res = document.querySelector('#resultado');

document.querySelector('#boton').addEventListener("click", sumar);
document.querySelector('#boton2').addEventListener("click", restar);

function sumar() {
    let suma = (+n1.value) + (+n2.value);
    res.value = `${suma}`;
}

function restar() {
    let resta = (+n1.value) - (+n2.value);
    res.value = `${resta}`;
}