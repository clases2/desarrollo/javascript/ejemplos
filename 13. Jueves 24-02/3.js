let n1 = document.querySelector('#numero1');
let n2 = document.querySelector('#numero2');
let res = document.querySelector('#resultado');

function operar(operacion) {
    let suma = (+n1.value) + (+n2.value);
    let resta = (+n1.value) - (+n2.value);

    if (operacion == 'sumar') {
        res.value = `${suma}`;
    } else {
        res.value = `${resta}`;
    }
}