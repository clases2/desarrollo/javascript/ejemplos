let salida = document.querySelector('#salida');

salida.style.width = "200px";

function cambiar(operacion) {
    if (operacion == 'rojo') {
        salida.style.backgroundColor = 'red';
    } else if (operacion == 'azul') {
        salida.style.backgroundColor = 'blue';
    } else if (operacion == 'verde') {
        salida.style.backgroundColor = 'green';
    } else if (operacion == 'aumentar') {
        salida.style.width = parseInt(salida.style.width) + 30 + "px";
    } else if (operacion == 'reducir') {
        salida.style.width = parseInt(salida.style.width) - 30 + "px";
    }
}