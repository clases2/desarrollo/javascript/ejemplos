window.addEventListener("load", () => {
    if (sessionStorage.getItem("dato") == null) {
        datos = [];
        sessionStorage.setItem("dato", JSON.stringify(datos));
    } else {
        datos = JSON.parse(sessionStorage.getItem("dato"));
    }


    datos.forEach(e => {
        document.querySelector('div').innerHTML += `<li>Nombre: ${e.nombre}</li>`
        document.querySelector('div').innerHTML += `<li>Edad: ${e.edad}</li>`
    });

    document.querySelector('#borra').addEventListener("click", () => {
        sessionStorage.removeItem("dato");
        document.querySelector('div').innerHTML = "";
        datos = [];
        sessionStorage.setItem("dato", JSON.stringify(datos));
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let texto = {
            nombre: document.querySelector('#nombre').value,
            edad: document.querySelector('#edad').value
        }

        datos = JSON.parse(sessionStorage.getItem("dato"));
        datos.push(texto);
        sessionStorage.setItem("dato", JSON.stringify(datos));
        document.querySelector('div').innerHTML += `<li>Nombre: ${texto.nombre}</li>`
        document.querySelector('div').innerHTML += `<li>Edad: ${texto.edad}</li>`
    })
})