window.addEventListener("load", function () {

    if (localStorage.getItem("datos") == null) {
        //Creamos un array y lo convertimos en string para poder guardarlo en la memoria local sin que se pierda su formato.
        datos = [];
        localStorage.setItem("datos", JSON.stringify(datos));
    } else {
        //Convertimos el string en el formato correspondiente.
        datos = JSON.parse(localStorage.getItem("datos"));
        //Como ya existen los datos, los imprimimos al cargar la pagina.
        datos.forEach(function (v) {
            document.querySelector('tbody').innerHTML += `
            <tr>
                <td>${v.nombre}</td>
                <td>${v.nif}</td>
                <td>${v.edad}</td>
            </tr>`;
        });
    }

    document.querySelector('#mas').addEventListener("click", function () {
        let persona = {
            nombre: document.querySelector('#nombre').value,
            nif: document.querySelector('#nif').value,
            edad: document.querySelector('#edad').value
        };
        //Cargamos el array guardado en la memoria local y le agregamos los datos del objeto 'personas'.
        datos = JSON.parse(localStorage.getItem("datos"));
        datos.push(persona);
        //Convertimos de nuevo el array en string para guardarlo en la memoria local.
        localStorage.setItem("datos", JSON.stringify(datos));

        //Creamos un tbody para separarlo del thead y pdoer limpiarlo mas adelante sin borrar el thead.
        document.querySelector('tbody').innerHTML +=
            `<tr>
                <td>${persona.nombre}</td>
                <td>${persona.nif}</td>
                <td>${persona.edad}</td>
            </tr>`;
    });

    document.querySelector('#borra').addEventListener("click", function () {
        //Limpiamos el array para poder usarlo otra vez despues de borrar.
        datos = [];
        localStorage.removeItem("datos");
        localStorage.setItem("datos", JSON.stringify(datos));
        document.querySelector('tbody').innerHTML = "";
    });

})