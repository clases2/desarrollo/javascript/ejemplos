function calcular() {
    let dato = null;
    let vocales = 0;

    dato = document.querySelector('#inombre').value; // Accedemos al contenido de la caja de texto.
    dato = dato.toLowerCase(); // Pasamos todo el texto a minusculas para facilitar el proceso.

    for (let cont = 0; cont < dato.length; cont++) { // Usamos el for para recorrer el dato letra por letra como si fuese un array.
        if (dato[cont] == 'a' || dato[cont] == 'e' || dato[cont] == 'i' || dato[cont] == 'o' || dato[cont] == 'u') {
            vocales++;
        }

/* Tambien se puede hacer con switch.
        switch (dato[cont]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                vocales++;
        }
*/

    }

    document.querySelector('#ivocales').value = vocales; // Accedo a la caja de texto vocales y la igualo a la variable para mostrar el numero.
}