// Creo una variable donde coloco la fecha actual
let fecha = new Date();

// Saco el dia de la semana de la fecha actual
let dia = fecha.getDay();

// Tengo un array con los li
let elementos = document.querySelectorAll('li');

// Controlo el problema con los domingos
if (dia == 0) {
    dia = 7;
}

elementos[dia - 1].style.opacity = 1;