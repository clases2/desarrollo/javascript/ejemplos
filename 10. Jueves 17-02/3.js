let max = 0;
let acumulador = ""; // Se asigna la variable como texto.

max = +prompt("Escribe un numero");

for (let cont = 1; cont <= max; cont++) {
    acumulador += cont; // Se usa un + para concatenar los contadores, no para sumarlos.
    document.write(`${acumulador}<br>`);
}