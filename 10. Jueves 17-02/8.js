// Utilizando if
/*let numero = +prompt("Escribe un numero del 1 al 7");
let dia = 0;

if (numero == 1) {
    dia = `Lunes`;
} else if (numero == 2) {
    dia = `Martes`;
} else if (numero == 3) {
    dia = `Miercoles`;
} else if (numero == 4) {
    dia = `Jueves`;
} else if (numero == 5) {
    dia = `Viernes`;
} else if (numero == 6) {
    dia = `Sabado`;
} else if (numero == 7) {
    dia = `Domingo`;
} else {
    alert("Error");
}
document.write(`${dia}`);
*/



// Utilizando arrays
let dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
let numero = +prompt("Escribe un numero del 1 al 7");

if (numero < 1 || numero > 7) {
    alert("Error");
} else {
    document.write(`El dia es ${dias[numero - 1]}`);
}




// Utilizando switch
/*let numero = +prompt("Escribe un numero del 1 al 7");
let dia = 0;
switch (numero) {
    case 1:
        dia = "Lunes";
        break;
    case 2:
        dia = "Martes";
        break;
    case 3:
        dia = "Miercoles";
        break;
    case 4:
        dia = "Jueves";
        break;
    case 5:
        dia = "Viernes";
        break;
    case 6:
        dia = "Sabado";
        break;
    case 7:
        dia = "Domingo";
        break;
    default:
        dia = 0;
        break;
}

if (dia == 0) {
    alert("Error");
} else {
    document.write(`${dia}`);
}*/
