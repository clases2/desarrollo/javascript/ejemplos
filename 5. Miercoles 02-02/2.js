var radio=0;
var perimetro=0;
var area=0;
let circulo=null; // Variable que apunta al div
const PI=3.14;

radio=prompt("Introduce el radio.", "0");

area=PI*Math.pow(radio, 2); // Math.pow(numero, exponente) o se escribe tambien: numero**exponente
perimetro=2*PI*radio;

document.write("Radio del circulo: " + radio );
document.write("<br>Area del circulo: " + area); 
document.write("<br>Perimetro del circulo: " + perimetro);

// Dibujar el circulo con el radio
circulo=document.querySelector("#circulo"); // El circulo se iguala a document.querySelector para ahorrar texto, escribir circulo va a ser como escribir document.querySelector
circulo.style.width=radio+"px";
circulo.style.height=radio+"px";
circulo.style.backgroundColor="blue";
circulo.style.borderRadius=radio+"px";