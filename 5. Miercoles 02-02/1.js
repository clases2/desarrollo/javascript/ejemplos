// Definimos las variables.
let base=0;
let altura=0;
let perimetro=0;
let area=0;

// Pedimos los datos
base=prompt("Introduce la BASE.");
altura=prompt("Introduce la ALTURA.");

// Procesamos los datos
perimetro= 2*base+2*altura;
area= base*altura;

//Mostramos los datos procesados en pantalla
document.write("El perimetro es: " + perimetro + "<br>El area es: " + area);