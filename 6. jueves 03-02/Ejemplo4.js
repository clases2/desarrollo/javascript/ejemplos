let numero1=0;
let numero2=0;
let numero3=0;

// Utilizar el parseInt para que el texto que lea en el HTML lo convierta a numero y poder operar mas adelante.
numero1=parseInt(document.querySelector("#numero1").innerHTML);
numero2=parseInt(document.querySelector("#numero2").innerHTML);
numero3=parseInt(document.querySelector("#numero3").innerHTML);

document.querySelector("#suma").innerHTML= `${numero1}+${numero2}+${numero3}=${numero1+numero2+numero3}`;
document.querySelector("#producto").innerHTML= `${numero1}*${numero2}*${numero3}=${numero1*numero2*numero3}`;
document.querySelector("#potencia").innerHTML= `${numero1}^${numero2}=${numero1**numero2}`;